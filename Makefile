clean:
	rm -rf node_modules dist

install: clean
	npm install

dev: install
	npm run dev

build: install
	npm run build

.PHONY: clean install start build
