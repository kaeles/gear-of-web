# Changelog

## 0.6

1. Really improvment of CSS and JS files size.
2. Fix templates layouts.
3. Style inputs 

## 0.5

### v0.5 nouveautés

1. Traduction des chaînes de caractères pour les scripts JS
2. Ajout des contextes dans les traductions
3. Bootstrap est désormais chargé via CDN
4. Webpack inclu Bootstrap comme librairie externe
5. Coding standars WordPress appliqués

### v0.5 fixs

1. La gallerie dans Gutunberg comprend un ID qui ne change plus
2. Refactorisation des templates pour une meilleure prise en charge Bootstrap

## 0.4

### Les nouveautés

1. Les principaux blocs de Gutenberg implémentent désormais les classes Bootstrap
2. Les images et les galeries gèrent maintenant la fonction lightbox (voir: <http://ashleydw.github.io/lightbox/>)
3. Gestion du menu mobile améliorée
4. Ajout d'un configurateur de couleurs pour le thème
5. Ajout d'un écran de configuration pour le référencement local
6. Améliore la gestion des classes Bootstrap dans les contenus
7. Nouvelles traductions

### Les fixs

1. Réduction de la tailles des résumés des articles qui étaient trop long sur les pages catégories
2. La pagination des pages 4040 est désormais fonctionnelle
3. Ajout d'une règle de sécurité dans l'améliorateur de contenu
4. Corrige le niveau de supperposition des sous-menus
5. Corrige : Trying to access array bool if customlogo does not exist

## 0.3

1. Création du thème enfant pour intégrer la charte graphique de l'association (v0.1)
2. Intégration de Webpack pour automatiser la gestion des fichiers distribués
3. Fusion et minification des fichiers JS et CSS distribués au navigateur (moins de requête HTTP, la planète dit merci)
4. Ajout du pré-processeur SASS
5. Ajout du support des auto-préfixeurs pour la rétrocompatibilité avec les anciens navigateurs
6. Charte graphique appliquée sur les menus

## 0.2

1. Thème prêt pour le multilingue
2. Ajout d'un formulaire de recherche dans le template 404
3. Traduction des chaînes de caractères des templates
4. Menu inline pour desktop et block pour mobile
5. Style général de bootstrap appliqué aux éléments de bases délivrés par WordPress et le thème

## 0.1

1. Tempalte WordPress de base
2. Intégration des styles de base de **Bootstrap**
3. Amélioration du contenu délivré par WordPress pour une optimisation SEO
4. Pagination optimisée SEO
5. Optimisation des balises head pour le SEO
6. Règles de sécurité de base
7. Suppression de la taxonomie "tag" pour optimisation SEO
8. Intégration des classes bootstrap dans Gutenberg (éditeur WordPress)
9. Images à la une supportées
10. Intégration du local business pour le SEO
11. Intégration des taxonomies suggerées. Diffère fonction de la taxonomie visitée
