import Navigation from "./site/navigation";
import ColorSwitcher from "./site/color-mode-switcher";

export default class Gof {
  name = "Gear of Web";
  navigation = new Navigation();
  colorSwitcher = new ColorSwitcher();

  get name() {
    return this.name;
  }

  get navigation() {
    return this.navigation;
  }

  get colorSwticher() {
    return this.colorSwitcher;
  }

  init() {
    // Console message.
    console.log(this.name + ": runtime init...");
    // Init the navigation menu.
    if (this.navigation.state === 1) {
      this.navigation.init();
      // Console message.
      console.log(this.name + ": navigation ready.");
    } else {
      // Console message.
      console.log(this.name + ": navigation module runtime error.");
    }

    window.gof = this;
  }
}

const gof = new Gof();
gof.init();
