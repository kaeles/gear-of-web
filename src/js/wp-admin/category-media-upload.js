/**
 * New field in taxonomies screens.
 *
 * Extends taxonomies screens behavior:
 * Adds a new field to upload an image used by the taxonomy (optional).
 *
 * @link https://rolling-web.fr
 * @file This file defines the new field in taxonomies screens.
 * @author Vincent Blée <vincent@rolling-web.fr>
 * @since 0.5
 */
jQuery(function ($) {
  // Instantiates the variable that holds the media library frame.
  var meta_image_frame;

  // Instantiates the variable that holds the imput button value.
  var upload_image_btn = $("#upload_image_btn").val();

  // Runs when the image button is clicked.
  $("#upload_image_btn").on("click", function (e) {
    // Prevents the default action from occuring.
    e.preventDefault();

    // If the frame already exists, re-open it.
    if (meta_image_frame) {
      meta_image_frame.open();
      return;
    }

    // Sets up the media library frame
    meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
      title: gof_i18n.category_extended_title,
      button: { text: gof_i18n.category_extended_button },
      library: { type: "image" },
    });

    // Runs when an image is selected.
    meta_image_frame.on("select", function () {
      // Grabs the attachment selection and creates a JSON representation of the model.
      var media_attachment = meta_image_frame
        .state()
        .get("selection")
        .first()
        .toJSON();

      // Sends the attachment URL to our custom image input field.
      $(".taxonomy-term-image-attach").val(media_attachment.url);
      $("#taxonomy-term-image-id").val(media_attachment.id);

      // Prepare image field that will be implemented in the DOM.
      var img =
        $("taxonomy-term-image-preview img").length == 0
          ? $("<img />")
          : $("taxonomy-term-image-preview img");
      img.attr("src", media_attachment.sizes.thumbnail.url);

      // Print the image preview.
      $("#taxonomy-term-image-preview").html(img);
    });

    // Opens the media library frame.
    meta_image_frame.open();
  });

  // Runs when the image remove button is clicked.
  $("#remove_image_btn").on("click", function (e) {
    // Prevents the default action from occuring.
    e.preventDefault();

    // If the frame already exists, close it.
    if (meta_image_frame) {
      meta_image_frame.close();
    }

    $(".taxonomy-term-image-attach").val(upload_image_btn);
    $("#taxonomy-term-image-preview").empty();
    $("#taxonomy-term-image-id").val("");
  });
});
