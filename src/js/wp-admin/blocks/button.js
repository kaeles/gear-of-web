/**
 * Unregister default style, then register bootstrap styles for button block.
 * @see https://developer.wordpress.org/block-editor/developers/filters/block-filters/
 */
wp.domReady ( function() {
    wp.blocks.unregisterBlockStyle( 'core/button', 'outline');
    wp.blocks.unregisterBlockStyle( 'core/button', 'fill');
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'bootstrap-primary',
        label: gof_i18n.block_button_primary_style_label,
        isDefault: true
    });
    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'bootstrap-secondary',
        label: gof_i18n.block_button_secondary_style_label,
    });
});
