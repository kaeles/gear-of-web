/**
 * Block core/image.
 *
 * Extends the block behavior :
 * Lightbox block style is now available.
 * Gives className to children elements to support Bootstrap.
 *
 * @link https://rolling-web.fr
 * @file This file defines the new image block behavior.
 * @author Vincent Blée <vincent@rolling-web.fr>
 * @since 0.4
 */

wp.domReady(() => {
  wp.blocks.registerBlockStyle('core/image', {
    name: 'lightbox',
    //label: gof_i18n.block_image_style_label,
    label: 'Lightbox'
  });
  wp.hooks.addFilter(
    'blocks.getSaveElement',
    'gof_theme/block-filters/image',
    addBootstrapImageSupport
  )
});

/**
 * The filter that applies to the result of the block’s save function.
 *
 * @param {react.element} element
 * @param {react.element} blockType
 * @return {react.element}
 */
function addBootstrapImageSupport(element, blockType, attributes) {
  if (blockType.name !== 'core/image') {
    return element;
  }

  // Initiates changes if the block is a valid React Component.
  if (React.isValidElement(element)) {
		attributes.lightbox = element.props.className.includes('is-style-lightbox') ? true : false;
		assignBootstrapClassName(element, attributes);
	}

  return element;
}

/**
 * Adds data-togle attributes on href tag.
 *
 * @param {react.element} element
 */
function assignLightboxDataToggle(element) {
		if (element.type === 'a') {
			Object.assign(element.props, {
				'data-toggle': `lightbox`
			});
		}
}

/**
 * Adds Bootstraps className on children elements.
 *
 * @param {react.element} element
 * @param {react.element.attributes} attributes
 */
function assignBootstrapClassName(element, attributes) {
	// Iterates the children of the block.
	wp.element.Children.map(element.props.children, function (child) {
		if (child !== null) {
			if (child.type === 'a') {
				// Enables Lightbox support if this block style has selected.
				if (attributes.lightbox == true) {
					assignLightboxDataToggle(child);
				} else { // or say bye bye to lightbox support.
					delete child.props['data-toggle'];
					delete attributes.lightbox;
				}
			}
			assignBootstrapClassName(child, attributes);
		}
	});
}
