/**
 * Block core/gallery.
 *
 * Extends the block behavior :
 * Lightbox block style is now available.
 * Gives className to children elements to support Bootstrap.
 *
 * @link https://rolling-web.fr
 * @file This file defines the new gallery block behavior.
 * @author Vincent Blée <vincent@rolling-web.fr>
 * @since 0.4
 */

wp.domReady(() => {
  wp.hooks.addFilter(
    'blocks.getSaveElement',
    'gof_theme/block-filters/gallery',
    addBootstrapGallerySupport
  );
});

/**
 * The filter that applies to the result of the block’s save function.
 *
 * @param {react.element} element
 * @param {react.element} blockType
 * @return {react.element}
 */
function addBootstrapGallerySupport(element, blockType, attributes) {
  if (blockType.name !== 'core/gallery') {
    return element;
  }

  // Current element must be a valid React Component.
  if (React.isValidElement(element)) {
    if (!attributes.hasOwnProperty('anchor')) {
      attributes.anchor = `gallery-${Math.floor(Math.random() * Math.floor(5))}`;
    }
    // Goes through the child elements and make the changes.
    assignBootstrapClassName(element, attributes);
  }

  return element;
}

/**
 * Adds Bootstrap className on each elements.
 * Also, adds data-togle attributes on href tag.
 *
 * @param {react.element} element
 * @param {react.element.attributes} attributes
 */
function assignBootstrapClassName(element, attributes) {
  // Initiates changes if the element is a valid React Component.
  if (React.isValidElement(element)) {
    // Iterates the children of the block.
    wp.element.Children.map(element.props.children, function (child) {
      if (child !== null) {
        // Adds figure className for Bootstrap support.
        if (child.type === 'figure') {
          Object.assign(child.props, {
            'className': `is-style-lightbox`
          });
        }
        // Enables Lightbox support on href tags.
        if (child.type === 'a') {
          Object.assign(child.props, {
            'data-toggle': `lightbox`,
            'data-gallery': attributes.anchor,
          });
          // Assigns a title in the frame if caption exists.
          if (attributes.images.length > 0) {
            const ID_image = child.props.children.props['data-id'];
            let caption = '';
            for (let image of attributes.images) {
              if (image.id == ID_image) {
                caption = image.caption;
              }
            }
            if (caption.length > 0) {
              Object.assign(child.props, {
                'data-title': caption,
              });
            }
          }
        }
        assignBootstrapClassName(child, attributes);
      }
    });
  }

}
