/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
export default class Navigation {
  state = 0;
  siteNavigation;
  button;
  menuWrapper;
  menu;
  extraMenu;
  itemsWithChildren;

  constructor() {
    this.siteNavigation = document.getElementById("site-navigation");

    if (this.siteNavigation) {
      this.button = this.siteNavigation.querySelector(".menu-toggle");
      this.menuWrapper = this.siteNavigation.querySelector(".menu-wrapper");
    }

    if (this.menuWrapper) {
      this.menu = this.menuWrapper.querySelector("#primary-menu");
      this.extraMenu = this.menuWrapper.querySelector("#extra-menu");
    }

    if (this.menu) {
      this.itemsWithChildren = this.menu.querySelectorAll(".has-children");
      this.state = 1; // Everything is done. The navigation menu is an element into the DOM.
    }
  }

  /**
   * Init the navigation.
   *
   */
  init() {
    // Attach event listener to the button.
    this.button.addEventListener("click", this.toggleMenu.bind(this));

    // Prevent webkit (Safari iPhone) 100vh issue.
    if (window.innerWidth < 1280) {
      this.menuWrapper.style.height = this.getViewPortHeight() + "px";
    }

    // Toggle focus each time a menu item with children receive a touch event.
    for (const item of this.itemsWithChildren) {
      item.addEventListener("click", Navigation.toggleFocus, false);
    }

    window.addEventListener("resize", this.resizeMenu.bind(this));
  }

  /**
   * Resize menu wrapper on resize.
   *
   */
  resizeMenu() {
    // Prevent webkit (Safari) 100vh issue.
    if (window.innerWidth < 1280) {
      this.menuWrapper.style.height = this.getViewPortHeight() + "px";
    } else {
      this.menuWrapper.style.height = "auto";
    }
  }

  /**
   * Close or open the menu according to its status.
   *
   */
  toggleMenu() {
    if (window.innerWidth < 1280) {
      document.body.classList.toggle("expanded-navbar");

      this.siteNavigation.classList.toggle("toggled");

      if (this.button.getAttribute("aria-expanded") === "true") {
        this.button.setAttribute("aria-expanded", "false");
      } else {
        this.button.setAttribute("aria-expanded", "true");
      }
    } else {
      document.body.classList.remove("expanded-navbar");

      this.siteNavigation.classList.remove("toggled");

      this.button.setAttribute("aria-expanded", "false");
    }
  }

  /**
   * Get viewport height available.
   * If the user is logged in, the height of the admin bar subtracts from the available height.
   *
   * @return {number} Available view height with px mention.
   */
  getViewPortHeight() {
    const wpAdminBar = document.getElementById("wpadminbar");
    let viewPortHeight = window.innerHeight - this.button.offsetHeight;

    if (document.body.contains(wpAdminBar)) {
      viewPortHeight -= wpAdminBar.offsetHeight;
    }
    return viewPortHeight;
  }

  /**
   * Sets or removes .focus class on an element.
   *
   * @param {Event} event - The event interface.
   */
  static toggleFocus(event) {
    event.stopPropagation();

    const currentItem = event.currentTarget;

    for (const item of currentItem.parentNode.children) {
      if (currentItem !== item) {
        item.classList.remove("focus");
      }
    }

    currentItem.classList.toggle("focus");
  }

  /**
   * Get the current state of navigation menu.
   *
   * @return {number} Current state.
   */
  get state() {
    return this.state;
  }
}
