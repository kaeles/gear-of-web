/**
 * File color-mode-swticher.js.
 *
 * Handles toggling the color mode (dark or light)
 */

export default class ColorSwitcher {
  use = true;

  constructor() {
    if (this.use == true) {
      if (this.getTheme() == null) {
        this.setTheme("light");
      } else if (this.getTheme() == "dark") {
        document.body.classList.add("dark");
        // Change switcher's visual state.
        if (document.getElementById("color-switcher")) {
          document.getElementById("color-switcher").classList.add('dark-mode');
        }
      }
    }
  }

  darkModeToggler() {
    // If the user's preference in localStorage is dark...
    if (this.getTheme() == "light") {
      // ...let's toggle the .dark-theme class on the body
      document.body.classList.add("dark");
      // Finally, let's save the current preference to localStorage to keep using it
      this.setTheme("dark");
      // Change switcher's visual state.
      document.getElementById("color-switcher").classList.add('dark-mode');
      // Otherwise, if the user's preference in localStorage is light...
    } else if (this.getTheme() == "dark") {
      // ...let's toggle the .light-theme class on the body
      document.body.classList.remove("dark");
      // Finally, let's save the current preference to localStorage to keep using it
      this.setTheme("light");
      // Change switcher's visual state.
      document.getElementById("color-switcher").classList.remove('dark-mode');
    }
  }

  createButtonSwitcher() {
    let button = document.createElement("button");
    button.setAttribute("aria-label", "Theme color switcher");
    button.setAttribute("id", "color-switcher");
    button.addEventListener("click", this.darkModeToggler.bind(this));
    document.body.appendChild(button);
  }

  setTheme(mode) {
    if (mode == "light" || "dark") {
      localStorage.setItem("theme", mode)
    }
  }

  getTheme() {
    return localStorage.getItem("theme");
  }

}
