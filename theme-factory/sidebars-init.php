<?php

defined( "ABSPATH" ) or die( "Cheatin&#8217; uh?" );

function gof_widgets_init() {
	register_sidebar( array(
		"name"          => _x( "Main menu Extra sidebar", "sidebar name", "gear-of-web" ),
		"id"            => "main-menu-sidebar-1",
		"description"	=> _x( "Show an additional widgets in the header navigation menu", "sidebar description", "gear-of-web" ),
		"before_widget" => '<div id="%1$s" class="widget %2$s">',
		"after_widget"  => "</div>",
		"before_title"  => "<span class='widget-title'>",
		"after_title"   => "</span>",
	) );

	if ( function_exists( "is_woocommerce_activated" ) ) {
		register_sidebar( array(
			"name"          => _x( "Main menu WooCommerce sidebar", "sidebar name", "gear-of-web" ),
			"id"            => "main-menu-sidebar-2",
			"description"	=> _x( "Show WooCommerce widgets in the header navigation menu", "sidebar description", "gear-of-web" ),
			"before_widget" => '<div id="%1$s" class="widget %2$s">',
			"after_widget"  => "</div>",
			"before_title"  => '<span class="widget-title">',
		"after_title"   => '</span>',
		) );
	}

	register_sidebar( array(
		"name"          => _x( "Single Sidebar", "sidebar name", "gear-of-web" ),
		"id"            => "single-sidebar-1",
		"before_widget" => '<div id="%1$s" class="widget %2$s">',
		"after_widget"  => "</div>",
		"before_title"  => '<span class="widget-title">',
		"after_title"   => '</span>',
	) );

	register_sidebar( array(
		"name"          => _x( "Page Sidebar", "sidebar name", "gear-of-web" ),
		"id"            => "page-sidebar-1",
		"before_widget" => '<div id="%1$s" class="widget %2$s">',
		"after_widget"  => "</div>",
		"before_title"  => '<span class="widget-title">',
		"after_title"   => '</span>',
	) );

	register_sidebar( array(
		"name"          => _x( "Top footer content", "sidebar name", "gear-of-web" ),
		"id"            => "top-footer",
		"description"	=> _x( "Add blocks to build your footer.", "footer sidebar description", "gear-of-web" ),
		"before_widget" => '<div id="%1$s" class="widget %2$s">',
		"after_widget"  => "</div>",
		"before_title"  => '<span class="widget-title">',
		"after_title"   => '</span>',
	) );
}
add_action( "widgets_init", "gof_widgets_init" );
