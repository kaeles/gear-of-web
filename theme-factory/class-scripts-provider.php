<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security.

/**
 * This file contains the class that enqueue GOF scripts and their dependencies.
 *
 * Gear_of_Web need several things to works :
 * - Bootstrap 4.1 https://getbootstrap.com/docs/4.1/getting-started/introduction/
 * - Popper.js (necessary for Bootstrap JS)
 * - EkkoLightbox (for images lightbox support)
 *
 * Admin and Front have different bundle scripts.
 * For the moment, Gear_Of_Web does not import Bootstrap in admin and especially not in Gutenberg.
 * This work is in progress and are not totally ready (bug issues).
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.1
 * @license 	http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author 		Rolling Web <contact@rolling-web.fr>
 */

/**
 * GOF_Script_Provider class manage styles and scripts of Gear_Of_Web theme.
 *
 * GOF_Script_Provider hooked several methods in wp_enqueue_scripts and admin_enqueue_scripts hooks.
 * You can remove specific actions if necessary.
 */
class GOF_Scripts_Provider {

	/**
	 * Initialize the Scripts Provider Manager.
	 *
	 * @access public
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public static function init() {
		// remove default blocks styles in the front side part.
		add_action( 'wp_enqueue_scripts', array( 'GOF_Scripts_Provider', 'remove_default_blocks_styles' ), 12 );

		// enqueue Gear_Of_Web styles and scripts in the front side part.
		add_action( 'wp_enqueue_scripts', array( 'GOF_Scripts_Provider', 'front_enqueue_gof_bundle' ), 14 );

		// insert inline theme colors variables.
		add_action( 'wp_enqueue_scripts', array( 'GOF_Scripts_Provider', 'front_insert_inline_styles' ), 15 );

		// enqueue Gear_of_Web styles and scripts in the admin side part.
		add_action( 'admin_enqueue_scripts', array( 'GOF_Scripts_Provider', 'admin_enqueue_gof_bundle' ), 8 );

		// Remove emojis support.
		//add_action( 'init', array( 'GOF_Scripts_Provider', 'disable_emojis' ) ); Temp disabled to follow ThemeReview recommendations.

	}

	/**
	 * Remove unnecessary wp-block styles..
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public static function remove_default_blocks_styles() {
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );

	}

	/**
	 * Enqueue Gear_Of_Web styles and scripts.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_Of_Web 0.6
	 *
	 * @uses GOF_THEME to get the theme version.
	 *
	 * @return void
	 */
	public static function front_enqueue_gof_bundle() {
		wp_enqueue_script( 'front', get_template_directory_uri() . '/dist/front.js', null, GOF_Theme::get_theme_version(), true );
		wp_enqueue_style( 'front', get_template_directory_uri() . '/dist/front.min.css', array( 'dashicons' ), GOF_Theme::get_theme_version() );
		wp_enqueue_script( 'comment-reply' );

	}

	/**
	 * Insert inline theme colors variables.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_Of_Web 0.6
	 *
	 * @return void
	 */
	public static function front_insert_inline_styles() {
		wp_add_inline_style( 'front', get_transient( 'gof_inline_styles' ) );

	}

	/**
	 * Enqueue Gear_of_Web styles and scripts in the admin side part.
	 *
	 * This part extends the categories screen by adding an image control.
	 * wp_enqueue_media is necessary to use the WordPress media frame.
	 * wp_localize_script allows to translate the JS script.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_Of_Web 0.6
	 *
	 * @return void
	 */
	public static function admin_enqueue_gof_bundle() {
		wp_enqueue_media();

		$gof_translations = apply_filters( 'gof_admin_localization', array(
			'category_extended_title' 				=> _x( 'Select or upload an image for this term', 'title of media frame to select an image for the taxonomy', 'gear-of-web' ),
			'category_extended_button' 				=> _x( 'Use this image', 'button label to select an image for the taxonomy', 'gear-of-web' ),
			'block_table_style_label' 				=> _x( 'Themed table', 'label style block', 'gear-of-web' ),
			'block_image_style_label'				=> _x( 'Lightbox', 'label style block', 'gear-of-web' ),
			'block_button_primary_style_label'		=> _x( 'Bootstrap Primary', 'label style block', 'gear-of-web' ),
			'block_button_secondary_style_label'	=> _x( 'Bootstrap Secondary', 'label style block', 'gear-of-web' ),
		) );

		wp_enqueue_script(
			'gof-admin', // handle ID
			get_template_directory_uri() . '/dist/gof-admin.min.js', // url of script
			array( 'jquery', 'thickbox', 'media-upload', 'wp-i18n', ), // dependencies
			GOF_Theme::get_theme_version(), // script version
			true // in the footer
		);

		wp_localize_script( 'gof-admin', 'gof_i18n', apply_filters( 'gof_admin_localize', $gof_translations ) );

		wp_enqueue_style(
			'gof-admin',
			get_template_directory_uri() . '/dist/gof-admin.min.css',
			array( 'dashicons' ), GOF_Theme::get_theme_version()
		);

		// loads Gear of Web css variables.
		wp_add_inline_style( 'gof-admin', get_transient( 'gof_inline_styles' ) );

	}

	/**
	 * Disable the emoji's
	 *
	 * Remove scripts, styles and other emojis support during the WP init process.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_Of_Web 0.6
	 *
	 * @return void
	 */
	public static function disable_emojis() {
		//remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		//remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		//remove_action( 'wp_print_styles', 'print_emoji_styles' );
		//remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	}
}

// END OF FILE
