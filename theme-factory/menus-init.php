<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security

/**
 * This file aim to register the navigation menus.
 *
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.1
 * @license 	http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author 		Rolling Web <contact@rolling-web.fr>
 * @see 		https://developer.wordpress.org/reference/functions/register_nav_menus/
 */

/**
 * Register menus navigation
 *
 * @since Gear_Of_Web 0.1
 *
 * @return void
 */
function gof_register_menus() {
	register_nav_menus(
		array(
			'header-menu' 	=> _x( 'Header Menu', 'menu location identifier', 'gear-of-web' ),
			'extra-menu' 	=> _x( 'Extra Menu', 'menu location identifier', 'gear-of-web' ),
		)
	);
}
add_action( 'after_setup_theme', 'gof_register_menus', 12 );
