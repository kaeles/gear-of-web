<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security.

/**
 * This file initialize the Gear_of_Web terms utility.
 *
 * Aim to obtain filtered terms according to the type of template used.
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.6
 * @license 	http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author 		Rolling Web <contact@rolling-web.fr>
 */

/**
 * GOF_Terms class provides WP_Term object or array of WP_Term.
 *
 * This a static class, you should not instantiate it.
 */
class GOF_Terms {

	/**
	 * Collect the six most used categories (first level).
	 *
	 * @access public
	 * @static
	 *
	 * @return WP_Term[]|int[]|string[]|string|WP_Error Array of terms, a count thereof as a numeric string, or WP_Error if any of the taxonomies do not exist.
	 */
	public static function get_main_terms() {
		$args = apply_filters( 'gof_main_terms_query_args', array(
			'taxonomy' 		=> 'category',
			'parent' 		=> 0,
			'orderby' 		=> 'count',
			'order' 		=> 'DESC',
			'number'		=> 6,
			'hide_if_empty'	=> true,
		) );

		return get_terms( $args );
	}

	/**
	 * Display a selector of none empty categories (first level).
	 *
	 * @access public
	 * @static
	 *
	 * @return string The HTML form select.
	 */
	public static function the_main_terms() {
		$args = apply_filters( 'gof_main_selector_terms_query_args', array(
			'taxonomy' 		=> 'category',
			'parent' 		=> 0,
			'orderby' 		=> 'count',
			'order' 		=> 'DESC',
			'number'		=> 6,
			'hide_if_empty'	=> true,
			'echo'			=> false,
			'class'			=> 'form-select',
		) );

		$dropdown = wp_dropdown_categories( $args );

		if ( "" !== $dropdown ) {
			echo sprintf(
				'<form id="category-select" class="category-select" action="%1$s" method="get">
					<div class="input-group">
						<label class="input-group-text" for="cat">%2$s:</label>
						%3$s
						<button type="submit" class="btn btn-outline">%4$s</button>
					</div>
				</form>',
				esc_url( home_url( '/' ) ),
				_x( 'Categories', 'Label for dropdown categories', 'gear-of-web' ),
				$dropdown,
				_x( 'Browse', 'Button label of categories selector', 'gear-of-web' )
			);
		}
	}

	/**
	 * Get the terms children.
	 *
	 * @access public
	 * @static
	 *
	 * @param WP_Term|int $term A WP_Term object or a term ID.
	 *
	 * @return WP_Term[]|int[]|string[]|string|WP_Error Array of terms, a count thereof as a numeric string, or WP_Error if any of the taxonomies do not exist.
	 */
	public static function get_terms_children( $term ) {
		if ( false === ( is_tax( $term ) || is_category( $term ) || is_int( $term ) ) ) {
			$error = new WP_Error(
				'gof-sibling-term',
				_x( "Term param is not an integer or WP_Term object!", 'Error log for developers', 'gear-of-web' )
			);
			return $error->get_error_message();
		}

		if ( is_int( $term ) ) $term = get_term( $term );

		$args = apply_filters( 'gof_children_terms_query_args', array(
			'taxonomy' 		=> $term->taxonomy,
			'exclude' 		=> $term->term_id,
			'parent'		=> $term->term_id,
			'orderby' 		=> 'count',
			'order' 		=> 'DESC',
			'hide_if_empty'	=> true,
		) );

		return get_terms( $args );
	}

	/**
	 * Display a selector of none empty categories (sub level of current term).
	 *
	 * @access public
	 * @static
	 *
	 * @param WP_Term|int $term A WP_Term object or a term ID.
	 *
	 * @return string The HTML form select.
	 */
	public static function the_terms_children( $term ) {
		if ( false === ( is_tax( $term ) || is_category( $term ) || is_int( $term ) ) ) {
			$error = new WP_Error(
				'gof-sibling-term',
				_x( "Term param is not an integer or WP_Term object!", 'Error log for developers', 'gear-of-web' )
			);
			return $error->get_error_message();
		}

		if ( is_int( $term ) ) $term = get_term( $term );

		$args = apply_filters( 'gof_children_terms_query_args', array(
			'taxonomy' 		=> $term->taxonomy,
			'exclude' 		=> $term->term_id,
			'parent'		=> $term->term_id,
			'orderby' 		=> 'count',
			'order' 		=> 'DESC',
			'hide_if_empty'	=> true,
			'echo'			=> false,
			'class'			=> 'form-select',
		) );

		$dropdown = wp_dropdown_categories( $args );
		if ( "" !== $dropdown ) {
			echo sprintf(
				'<form id="category-select" class="category-select" action="%1$s" method="get">
					<div class="input-group">
						<label class="input-group-text" for="cat">%2$s:</label>
						%3$s
						<button type="submit" class="btn btn-outline">Browse</button>
					</div>
				</form>',
				esc_url( home_url( '/' ) ),
				_x( 'Categories', 'Label for dropdown categories', 'gear-of-web' ),
				$dropdown,
			);
		}
	}
}
