<?php

defined('ABSPATH') or die('Cheatin&#8217; uh?'); // security

/**
 * Gear of Web walker comments implementation.
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.6
 * @license 	GPL2 - <https://www.gnu.org/licenses/gpl-2.0.html>
 *
 */

if ( !class_exists('GOF_Walker_Comments') ) :

	/**
	 * Main WP Bootstrap Comments Walker Class
	 *
	 * @since 0.1.0
	 */
	class GOF_Walker_Comments extends Walker_Comment {
		/**
		 * Start the element output.
		 *
		 * This opens the comment.  Will check if the comment has children or is a stand-alone comment.
		 *
		 * @access public
		 * @since 0.1.0
		 *
		 * @see Walker::start_el()
		 * @see wp_list_comments()
		 *
		 * @global int        $comment_depth
		 * @global WP_Comment $comment
		 *
		 * @param string $output  Passed by reference. Used to append additional content.
		 * @param object $comment Comment data object.
		 * @param int    $depth   Depth of comment in reference to parents.
		 * @param array  $args    An array of arguments.
		 */
		public function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
			$depth++;
			$GLOBALS['comment_depth'] = $depth;
			$GLOBALS['comment'] = $comment;

			if ( !empty( $args['callback'] ) ) {
				ob_start();
				call_user_func( $args['callback'], $comment, $args, $depth );
				$output .= ob_get_clean();
				return;
			}

			if ( ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) && $args['short_ping'] ) {
				ob_start();
				$this->ping( $comment, $depth, $args );
				$output .= ob_get_clean();
			} elseif ( 'html5' === $args['format'] ) {
				ob_start();
				if ( !empty( $args['has_children'] ) ) {
					$this->start_parent_html5_comment( $comment, $depth, $args );
				} else {
					$this->html5_comment( $comment, $depth, $args );
				}
				$output .= ob_get_clean();
			} else {
				ob_start();
				$this->comment( $comment, $depth, $args );
				$output .= ob_get_clean();
			}
		}


		/**
		 * Ends the element output, if needed.
		 *
		 * This ends the comment.  Will check if the comment has children or is a stand-alone comment.
		 *
		 * @access public
		 * @since 0.1.0
		 *
		 * @see Walker::end_el()
		 * @see wp_list_comments()
		 *
		 * @param string     $output  Passed by reference. Used to append additional content.
		 * @param WP_Comment $comment The comment object. Default current comment.
		 * @param int        $depth   Depth of comment.
		 * @param array      $args    An array of arguments.
		 */
		public function end_el( &$output, $comment, $depth = 0, $args = array() ) {
			if ( !empty( $args['end-callback'] ) ) {
				ob_start();
				call_user_func( $args['end-callback'], $comment, $args, $depth );
				$output .= ob_get_clean();
				return;
			}

			if ( !empty( $args['has_children'] ) && 'html5' === $args['format'] ) {
				ob_start();
				$this->end_parent_html5_comment( $comment, $depth, $args );
				$output .= ob_get_clean();
			} else {
				if ( 'div' == $args['style'] ) {
					$output .= "</div><!-- #comment-## -->\n";
				} else {
					$output .= "</li><!-- #comment-## -->\n";
				}
			}
		}


		/**
		 * Output the beginning of a parent comment in the HTML5 format.
		 *
		 * Bootstrap media element requires child comments to be nested within the parent media-body.
		 * The original comment walker writes the entire comment at once, this method writes the opening
		 * of a parent comment so children comments can be nested within.
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @see http://getbootstrap.com/components/#media
		 * @see wp_list_comments()
		 *
		 * @param object $comment Comment to display.
		 * @param int    $depth   Depth of comment.
		 * @param array  $args    An array of arguments.
		 */
		protected function start_parent_html5_comment( $comment, $depth, $args ) {
			$this->html5_comment( $comment, $depth, $args, $is_parent = true );
		}


		/**
		 * Output a comment in the HTML5 format.
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @see wp_list_comments()
		 *
		 * @param object  $comment   Comment to display.
		 * @param int     $depth     Depth of comment.
		 * @param array   $args      An array of arguments.
		 * @param boolean $is_parent Flag indicating whether or not this is a parent comment
		 */
		protected function html5_comment( $comment, $depth, $args, $is_parent = false ) {
			$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';

			$type = get_comment_type();

			$comment_classes = array();

			$comment_classes[] = '';

			// if it's a parent
			if ( $this->has_children ) {
				$comment_classes[] = 'parent';
				$comment_classes[] = 'has-children';
			}

			// if it's a child
			if ( $comment->comment_parent > 0 ) {
				$comment_classes[] = 'child';
				$comment_classes[] = 'has-parent';
				$comment_classes[] = 'parent-' . $comment->comment_parent;
			}

			$comment_classes = apply_filters( 'wp_bootstrap_comment_class', $comment_classes, $comment, $depth, $args );

			$class_str = implode( ' ', $comment_classes );

			?>
			<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $class_str, $comment ); ?>>
				<div class="comment-wrapper">
					<!-- header -->
					<div class="comment-header">
						<?php if ( 0 != $args['avatar_size'] && 'pingback' !== $type && 'trackback' !== $type ) { ?>
							<div class="author-img"><?php echo $this->get_comment_author_avatar( $comment, $args ); ?></div>
						<?php }; ?>
						<div class="comment-meta">
							<b><?php echo get_comment_author_link( $comment ); ?></b>
							<div class="time">
								<time datetime="<?php comment_time( 'c' ); ?>">
									<?php
									/* translators: 1: comment date, 2: comment time */
									printf( _( '%1$s at %2$s' ), get_comment_date('', $comment), get_comment_time() );
									?>
								</time>
							</div>
						</div>
					</div>

					<div class="comment-body">
						<?php comment_text(); ?>
					</div>

					<div class="comment-footer">
						<?php $this->comment_reply_link( $comment, $depth, $args, $add_below = 'reply-comment' ); ?>
						<?php if ( '0' == $comment->comment_approved ) : ?>
								<p class="comment-awaiting-moderation"><?php echo _('Your comment is awaiting moderation.' ); ?></p>
						<?php endif; ?>
					</div>

				<?php if ( $is_parent ) { ?>
					<div class="child-comments">
				<?php } else { ?>
				</div>
				<?php } ?>
		<?php
		}

		/**
		 * Output the end of a parent comment in the HTML5 format.
		 *
		 * Bootstrap media element requires child comments to be nested within the parent media-body.
		 * The original comment walker writes the entire comment at once, this method writes the end
		 * of a parent comment so child comments can be nested within.
		 *
		 * @see http://getbootstrap.com/components/#media
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @see wp_list_comments()
		 *
		 * @param object $comment Comment to display.
		 * @param int    $depth   Depth of comment.
		 * @param array  $args    An array of arguments.
		 */
		protected function end_parent_html5_comment( $comment, $depth, $args ) {
			$tag = ('div' === $args['style']) ? 'div' : 'li';
			?>
			</div><!-- /.child-comments -->
			</div><!-- /.media-body -->
			</<?php echo $tag; ?>><!-- /.parent -->

			<?php
		}


		/**
		 * Output a pingback comment.
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @see wp_list_comments()
		 *
		 * @param WP_Comment $comment The comment object.
		 * @param int        $depth   Depth of comment.
		 * @param array      $args    An array of arguments.
		 */
		protected function ping( $comment, $depth, $args ) {
			$tag = ( 'div' == $args['style'] ) ? 'div' : 'li';

			$comment_classes = array();
			$comment_classes[] = 'media';

			$comment_classes = apply_filters('wp_bootstrap_comment_class', $comment_classes, $comment, $depth, $args);

			$class_str = implode(' ', $comment_classes);
			?>
			<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class($class_str, $comment); ?>>
				<div class="comment-body">
					<div class="media-body">
						<?php echo _( 'Pingback:' ); ?> <?php comment_author_link($comment); ?> <?php edit_comment_link( _( 'Edit' ), '<span class="edit-link">', '</span>'); ?>
					</div><!-- /.media-body -->
				</div><!-- /.comment-body -->
			<?php
		}


		/**
		 * Generate avatar markup
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @param object $comment Comment to display.
		 * @param array  $args    An array of arguments.
		 */
		protected function get_comment_author_avatar( $comment, $args ) {
			$avatar_string = get_avatar( $comment, $args['avatar_size'] );

			return $avatar_string;
		}


		/**
		 * Displays the HTML content for reply to comment link.
		 *
		 * @access protected
		 * @since 0.1.0
		 *
		 * @param object $comment   Comment being replied to. Default current comment.
		 * @param int    $depth     Depth of comment.
		 * @param array  $args      An array of arguments for the Walker Object
		 * @param string $add_below The id of the element where the comment form will be placed
		 */
		protected function comment_reply_link( $comment, $depth, $args, $add_below = 'div-comment' ) {
			$type = get_comment_type();

			if ('pingback' === $type || 'trackback' === $type) {
				return;
			}

			comment_reply_link( array_merge( $args, array(
				'add_below' => $add_below,
				'depth'     => $depth,
				'max_depth' => $args['max_depth'],
				'before'    => '<div id="reply-comment-' . $comment->comment_ID . '" class="reply">',
				'after'     => '</div>'
			) ) );
		}

		/**
		 * Get comments reply form arguments.
		 *
		 * Use as 'comment_form' argument.
		 *
		 * @access public
		 * @static
		 * @since 4.3.0
		 *
		 * @return array array of comment fileds.
		 */
		public static function get_comment_args() {
			return apply_filters( 'gof_comment_args', array(
				'comment_field' =>
					'<div class="comment-form-comment form-group">
							<label for="comment">' . _x( 'Comment', 'comment label in comment form', 'gear-of-web' ) . '<span class="required">*</span></label>
							<textarea class="form-control comment-editor" id="comment" name="comment" aria-required="true"></textarea>
					</div>',
				'fields' => array(
					'author' =>
						'<div class="comment-form-author form-group">
							<label for="author">' . _x( 'Name', 'name label in comment form', 'gear-of-web' ) . '<span class="required">*</span></label>
							<input class="form-control" id="author" name="author" type="text" value="" size="30" maxlength="245" required="required">
						</div>',
					'email' =>
						'<div class="comment-form-author form-group">
							<label for="email">' . _x( 'Email', 'email label in comment form', 'gear-of-web' ) . '<span class="required">*</span></label>
							<input class="form-control" id="email" name="email" type="text" value="" size="30" maxlength="100" aria-describedby="email-notes" required="required">
						</div>',
					'url' =>
						'<div class="comment-form-url form-group">
							<label for="url">' . _x( 'Website', 'website label in comment form', 'gear-of-web' ) . '</label>
							<input class="form-control" id="url" name="url" type="text" value="" size="30" maxlength="200">
						</div>',
					'cookies' =>
						'<div class="comment-form-cookies-consent form-group">
							<div> ' . _x( 'Cookies', 'cookie title in comment form', 'gear-of-web' ) . '</div>
							<div class="form-check">
								<input class="form-check-input" id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" size="30" maxlength="200">
								<label class="form-check-label" for="wp-comment-cookies-consent">' . _x( 'Save my name, email, and website in this browser for the next time I comment.', 'cookie label in comment form', 'gear-of-web' ) . '</label>
							</div>
						</div>',
				),
				'submit_field' => '<div class="form-submit form-group">%1$s %2$s</div>'
			) );
		}

		/**
		 * filter submit button output for bootstrap support.
		 *
		 * Define the comment_form_submit_button callback. Use with
		 * the comment_form_submit_button WP filter.
		 *
		 * @access public
		 * @static
		 * @since 4.3.0
		 *
		 * @param string $submit_button
		 * @param array $args
		 */
		public static function filter_comment_form_submit_button( $submit_button, $args ) {
			// Override button class to support Bootstrap:
			$args['class_submit'] = 'btn btn-primary';

			$submit_before = '<div class="form-group">';
			$submit_after = '</div>';

			// Override the submit button HTML:
			$submit_button = $submit_before . '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />' . $submit_after;
			return sprintf(
				$submit_button,
				esc_attr( $args['name_submit'] ),
				esc_attr( $args['id_submit'] ),
				esc_attr( $args['class_submit'] ),
				esc_attr( $args['label_submit'] )
			);
		}

		/**
		 * filter reply button output for bootstrap support.
		 *
		 * Define the comment_reply_button CSS classes. Use with
		 * the comment_reply_link WP filter.
		 *
		 * @access public
		 * @static
		 * @since 4.3.0
		 *
		 * @param string $link The HTML markup for the comment reply link.
		 * @return string $link The new HTML markup for the comment reply link.
		 */
		public static function filter_comment_reply_button( $link ) {
			$link =  str_replace( "class='comment-reply-link'", "class='comment-reply-link btn btn-primary'", $link );
			return $link;
		}
	}

endif;
