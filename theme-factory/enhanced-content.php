<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

//customize excerpt length
function gof_excerpt_length( $length ) {
	return 55;
}
add_filter( 'excerpt_length', 'gof_excerpt_length', 999 );

//customize custom excerpt length
function gof_custom_excerpt_length( $excerpt ) {
	return wp_trim_excerpt();
}
add_filter( 'get_the_excerpt', 'gof_custom_excerpt_length', 999 );

function gof_init_theme_support() {
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'gof_init_theme_support' );

function gof_change_logo_class( $html ) {
	$html = str_replace( 'custom-logo-link', 'logo', $html );
	$html = str_replace( 'custom-logo', 'logo-img', $html );

	return $html;
}
add_filter( 'get_custom_logo', 'gof_change_logo_class' );


/**
 * Filter the main query for blog and home pages.
 *
 * By default, WordPress display twice the stickies posts : at top position and in their original position.
 * We want to display only once the stickies posts. So we need to filter the main query for home and blog pages.
 *
 * @since Gear_Of_Web 0.6
 *
 * @return WP_Query $query The main filtered query.
 */
function gof_remove_stickies_posts_from_main_query( $query ) {
	if ( $query->is_main_query() && ! is_admin() && is_home() ) {
		$query->set( 'post__not_in', get_option( 'sticky_posts' ) );
	}
	return $query;
}
add_action( 'pre_get_posts', 'gof_remove_stickies_posts_from_main_query' );

/**
 * Custom query to get only stickies posts.
 *
 * The main query is filtered by GOF_SEO::remove_stickies_posts_from_main_query.
 * If you want to get stickies posts, use this methods.
 *
 * @since Gear_Of_Web 0.6
 *
 * @return WP_Query $query Custom query that fetch stickies posts.
 */
function gof_fetch_stickies_posts() {
	$sticky = get_option( 'sticky_posts' );
	rsort( $sticky ); // Sort Sticky Posts, newest at the top.

	$args = apply_filters( 'gof_stickiesposts_custom_query_args', array(
		'post__in' => $sticky,
		'ignore_sticky_posts' => 1
	) );

	$query = new WP_Query( $args );

	return $query;
}

/**
 * Filter Gutenberg the available blocks.
 * Deleting some blocks may be unnecessary when editing the page.
 * The child theme can filter this rule again. Check the priority of the filter.
 */
function gof_allowed_block_types( $allowed_blocks ) {
	return array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
		'core/quote',
		'core/columns',
		'core/buttons',
		'core/button'
	);
}
add_filter( 'allowed_block_types', 'gof_allowed_block_types', 10 );
