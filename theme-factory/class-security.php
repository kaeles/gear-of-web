<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

/**
 * This file uses WP filters to enhanced basic security rules.
 *
 * Does many things :
 * - enhances the file names sanitizing rules
 * - improves the login error message (username or password invalid)
 * - removes critical author informations from body classes
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.1
 * @license 	http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author 		Rolling Web <contact@rolling-web.fr>
 */

/**
 * GOF_Security class.
 *
 * Some basics security rules are added with this class.
 *
 * @since Gear_of_Web 0.6
 */
class GOF_Security {
	/**
	 * Instantiate the security manager.
	 *
	 * Calls the concerning WP hooks or filters.
	 *
	 * @access public
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public static function init() {
		// add more characters to sanitize in filenames.
		add_filter( 'sanitize_file_name_chars', array( 'GOF_Security', 'sanitize_file_name_chars' ), 10, 1 );

		// filters the filename by adding more rules.
		add_filter( 'sanitize_file_name', array( 'GOF_Security', 'sanitize_file_name' ), 10, 1 );

		// change message error when login failed.
		add_filter( 'login_errors', array( 'GOF_Security', 'login_error_message' ) );

		// remove author name and ID in body_class function.
		add_filter( 'body_class', array( 'GOF_Security', 'clean_body_class' ), 10, 2 );

	}

	/**
	 * Add more characters to filter.
	 *
	 * @since 0.1
	 *
	 * @access public
	 * @static
	 *
	 * @uses sanitize_file_name_chars WP filter.
	 *
	 * @param array $special_chars Default WP special_chars rule.
	 *
	 * @return $special_chars Custom special_chars rule.
	 */
	public static function sanitize_file_name_chars( $special_chars = array() ) {
		// Add more characters to filter
		$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', '€' ), $special_chars );

		// Return sanitized data
		return apply_filters( 'gof_sanitize_filename_chars', $special_chars );

	}

	/**
	 * Filters the filename by adding more rules :
	 * - only lowercase
	 * - replace _ by -
	 *
	 * @since 0.1
	 *
	 * @access public
	 * @static
	 *
	 * @uses sanitize_file_name WP filter.
	 *
	 * @param string $file_name
	 *
	 * @return string the filename with its extension.
	 */
	public static function sanitize_file_name( $file_name ) {
		// get extension.
		preg_match( '/\.[^\.]+$/i', $file_name, $ext );

		if ( 0 == count( $ext ) ) {
			return $file_name;
		}

		$ext = $ext[0];

		// work only on the filename without extension.
		$file_name = str_replace( $ext, '', $file_name );

		// remove accents + only lowercase.
		$file_name = sanitize_title( $file_name );

		// replace _ by -
		$file_name = str_replace( '_', '-', $file_name );

		return $file_name . $ext;
	}

	/**
	 * Change message error when login failed.
	 *
	 * @since 0.1
	 *
	 * @access public
	 * @static
	 *
	 * @uses login_errors WP filter.
	 *
	 * @return string The message to return if the login attempt fails.
	 */
	public static function login_error_message() {
		return _x( 'Invalid username or password', 'login error message', 'gear-of-web' );
	}

	/**
	 * Remove author name and ID in body_class function.
	 *
	 * @since 0.1
	 *
	 * @access public
	 * @static
	 *
	 * @uses body_class WP filter.
	 *
	 * @return array All body_class without author ID and author name.
	 */
	public static function clean_body_class( $wp_classes, $extra_classes ) {
		if ( is_author() ) {
			// Getting author Information.
			$curauth = get_query_var( 'author_name' ) ? get_user_by( 'slug', get_query_var( 'author_name' ) ) : get_userdata( get_query_var( 'author' ) );
			// Blacklist author-ID class.
			$blacklist[] = 'author-'.$curauth->ID;
			// Blacklist author-nicename class.
			$blacklist[] = 'author-'.$curauth->user_nicename;
			// Delete useless classes.
			$wp_classes = array_diff( $wp_classes, $blacklist );
		}
		// Return all classes.
		return apply_filters( 'gof_body_classes', array_merge( $wp_classes, (array) $extra_classes ) );

	}

}

// END OF FILE
