<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security.

/**
 * Customize API: WP_Customize_Color_Control class
 *
 * Adds new section, setting and control to the Customizer.
 *
 * @package WordPress
 * @subpackage Gear_of_Web
 * @since Gear_of_Web 0.4
 */

/**
 * Customize Template Control class.
 *
 * @since Gear_of_Web 0.6
 *
 * @see WP_Customize_Manager
 */
class GOF_Theme_Templates_Customizer {
	/**
	 * Instantiate the object.
	 *
	 * Register customizer panel, sections and options.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public static function init() {
		add_action( 'customize_register', array( 'GOF_Theme_Templates_Customizer', 'register' ) );
	}

	/**
	 * Register customizer options.
	 *
	 * Adds a new section named "Theme colors settings".
	 * In this section, add the theme color settings.
	 * Each setting is controlled by the hex color sanitizer.
	 *
	 * @access public
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
	 *
	 * @return void
	 */
	public static function register( $wp_customize ) {
		// register new panel in the customizer.
		self::add_panel( $wp_customize );

		// register new sections in the customizer (gof_404_template, gof_blog_template, gof_search_template).
		self::add_sections( $wp_customize );

		// register new options in the new sections.
		self::add_options( $wp_customize );

	}

	/**
	 * Register new panel.
	 *
	 * This panel allow authorized users to manage default WP templates.
	 * WP do not offer the possibility of customizing general templates such as
	 * 404 templates, blog, search, ect ...
	 * Here, we offer the possibility to add a banner image for each template.
	 * Caution: using large images increases the loading time of a site. We are warning users of this.
	 *
	 * @access private
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @param WP_Customize_Manager $wp_customize WP Customizer object.
	 *
	 * @return void
	 */
	private static function add_panel( $wp_customize ) {
		$wp_customize->add_panel( 'gof_templates_settings', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => _x( 'Customize general templates', 'panel title in customizer', 'gear-of-web' ),
			'description'    => _x( 'Here, you can change theme default behavior for 404, blog or search templates.', 'panel description in customizer', 'gear-of-web' ),
		) );
	}

	/**
	 * Add new sections.
	 *
	 * Add new sections allow authorized users to customize the theme.
	 * A new filter is applied to allow developers to add new sections here.
	 *
	 * @access private
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @see add_filter
	 *
	 * @param WP_Customize_Manager $wp_customize WP Customizer object.
	 *
	 * @return void
	 */
	private static function add_sections( $wp_customize ) {
		// apply a new filter for developers. With this, you can register more sections.
		$sections = apply_filters( 'gof_customize_template_sections', array(
			'gof_404_template' => array(
				'title'         	=> _x( '404', 'section title in customizer', 'gear-of-web' ),
				'description'   	=> _x( '404 pages configuration', 'section description in customizer', 'gear-of-web' ),
				'panel'         	=> 'gof_templates_settings',
			),
			'gof_blog_template' => array(
				'title'         	=> _x( 'Blog', 'section title in customizer', 'gear-of-web' ),
				'description'   	=> _x( 'Blog pages configuration', 'section description in customizer', 'gear-of-web' ),
				'panel'         	=> 'gof_templates_settings',
			),
			'gof_search_template' => array(
				'title'         	=> _x( 'Search', 'section title in customizer', 'gear-of-web' ),
				'description'   	=> _x( 'Search pages configuration', 'section description in customizer', 'gear-of-web' ),
				'panel'         	=> 'gof_templates_settings',
			)
		) );

		// Iterate and register the new sections.
		foreach( $sections as $section_id => &$params ) {
			$wp_customize->add_section( $section_id, $params );
		}
		unset( $params ); // destroy reference on the last element.
	}

	/**
	 * Add new options.
	 *
	 * Add new options to allow athorized uers add a banner image on each supported template.
	 * A new filter is applied to allow developers add new sections here.
	 *
	 * @access private
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @see add_filter
	 *
	 * @param WP_Customize_Manager $wp_customize WP Customizer object.
	 *
	 * @return void
	 */
	private static function add_options( $wp_customize ) {
		$options = apply_filters( 'gof_customize_template_options', array(
			// 404 header background image
			array(
				'slug'          => 'gof_404_header_title',
				'default'       => null,
				'label'         => _x( '404 header title', 'option label in customizer', 'gear-of-web' ),
				'description'   => _x( 'Using an image for this section is not recommended. Your image will increase the page load time.', 'option description in customizer', 'gear-of-web' ),
				'section'       => 'gof_404_template',
			),
			// Blog header background image
			array(
				'slug'          => 'gof_blog_header_title',
				'default'       => null,
				'label'         => _x( 'Blog header title', 'option label in customizer', 'gear-of-web' ),
				'description'   => _x( 'Using an image for this section is not recommended. Your image will increase the page load time.', 'option description in customizer', 'gear-of-web' ),
				'section'       => 'gof_blog_template',
			),
			// Search header background image
			array(
				'slug'          => 'gof_search_header_title',
				'default'       => null,
				'label'         => _x( 'Search header title', 'option label in customizer', 'gear-of-web' ),
				'description'   => _x( 'Using an image for this section is not recommended. Your image will increase the page load time.', 'option description in customizer', 'gear-of-web' ),
				'section'       => 'gof_search_template',
			)
		) );

		foreach( $options as $i => &$option ) {
			$wp_customize->add_setting(
				$option['slug'],
				array(
					'default'           => $option['default'],
					'sanitize_callback' => 'esc_url_raw',
					'capability'        => 'edit_theme_options',
				)
			);
			$wp_customize->add_control(
				new WP_Customize_Image_Control(
					$wp_customize,
					$option['slug'],
					array(
						'label'         => $option['label'],
						'section'       => $option['section'],
						'settings'      => $option['slug'],
						'description'   => $option['description'],
					)
				)
			);
		}
		unset( $params ); // destroy reference on the last element.
	}
}

// END OF FILE.
