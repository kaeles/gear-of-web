<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

function gof_filter_category_dropdown_widget( $cat_args, $instance ) {
	$cat_args['class'] = 'form-select';
	return $cat_args;
}
add_filter( 'widget_categories_dropdown_args', 'gof_filter_category_dropdown_widget', 10, 2 );
