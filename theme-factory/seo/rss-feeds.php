<?php

defined('ABSPATH') or die('Cheatin&#8217; uh?');

/**
 * Increase RSS Length
 *
 * @since  1.0
 * @author  Vincent Blée - Rolling Web
 *
 * @hook excerpt_length, 1000
 *
 * @global  (void)
 * @param   (integer) $length Excerpt length (default to 55)
 * @return  (integer) $length Excerpt length
 */
function gof_rss_length( $length ) {
	// Only on feeds
	if ( is_feed() ) {
		$length = 90;
	}
	// Return value
	return $length;
}
add_filter( 'excerpt_length', 'gof_rss_length', 1000 );

/**
 * RSS Content
 *
 * Add thumbnail to RSS feeds
 *
 * @since  1.0
 * @author  Vincent Blée - Rolling Web
 *
 * @hook the_excerpt_rss
 *
 * @global 	WP_Post $post The global `$post` object.
 * @param 	(string)  $content Excerpt content
 * @return  (string)  $content Excerpt content
 */
function gof_rss_content( $content ) {
	// Retrieve Post Data through $post global
	global $post;
	// Add thumbnail if available
	if ( has_post_thumbnail( $post->ID ) ) {
		$content =  '<div>' . get_the_post_thumbnail( $post->ID, 'thumbnail' ) . '</div>' . $content;
	}
	/**
	 * RSS excerpt content Filter
	 *
	 * @since  1.0
	 * @author  Vincent Blée - Rolling Web
	 *
	 * @global  (void)
	 * @param 	(integer) $render Excerpt Content
	 * @return  (integer) $render Excerpt Content
	 */
	return apply_filters( 'gof_filter_rss_content', $content );
}
add_filter( 'the_excerpt_rss', 'gof_rss_content' );

/**
 * Add better anchor for Read More Links on RSS Feed
 *
 * @since  1.0
 * @author  Vincent Blée - Rolling Web
 *
 * @hook excerpt_more, 100
 *
 * @global 	WP_Post $post The global `$post` object.
 * @param 	(string) $default Defaut excerpt ellipse
 * @return  (string) $default New excerpt ellipse with link
 */
function gof_rss_read_more_content( $default ) {
	// Only for feeds
	if ( is_feed() ) {
		//  Get Post Data
		global $post;
		// "Read More" string
		$read_more_text = esc_html_x( 'Read more', 'Read more link on RSS feeds', 'gear-of-web' );
		// Get default excerpt end
		$excerpt_default_ellipse = ' [&hellip;]';
		// Enhance HTML markup
		$excerpt_more = $excerpt_default_ellipse . '<br><strong>' . $read_more_text . '</strong>';
		/**
		 * RSS excerpt "Read More" Filter
		 *
		 * @since  1.0
		 * @author  Vincent Blée - Rolling Web
		 *
		 * @global  (void)
		 * @param 	(integer) $excerpt_more_begin Excerpt More text Value before link
		 * @return  (integer) $excerpt_more_begin Excerpt More text Value before link
		 */
		$excerpt_more = apply_filters( 'gof_filter_rss_read_more_content', $excerpt_more );
		// "Read More" link
		$excerpt_more_link 	= '<a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . '</a>';
		// Render final "Read More"
		$default = $excerpt_more . $excerpt_more_link;
	}
	// return final data
	return $default;
}
add_filter( 'excerpt_more', 'gof_rss_read_more_content', 100, 1 );

/**
 * Clean RSS feeds
 *
 * @since 1.0.0
 *
 * @hook after_setup_theme 10
 *
 * @return void.
 */
function gof_rss_cleaning() {
	/**
	 * Removes Emoji in RSS
	 *
	 * @since 1.0.0
	 *
	 * @hook comment_text_rss
	 *
	 * @return void.
	 */
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	/**
	 * Prevent showing WordPress version in RSS Feed
	 *
	 * @uses __return_false() https://developer.wordpress.org/reference/functions/__return_false/
	 *
	 * @since   1.0
	 * @author  Vincent Blée - Rolling Web
	 *
	 * @hook get_the_generator_rss2
	 * @hook get_the_generator_atom
	 */
	add_filter( 'get_the_generator_rss2', '__return_false' );
	add_filter( 'get_the_generator_atom', '__return_false' );
}
add_action( 'after_setup_theme', 'gof_rss_cleaning', 1 );
