<?php

add_theme_support( 'editor-styles' );
add_editor_style( 'dist/editor.min.css' );

// Allow the wide alignment an image.
add_theme_support( 'align-wide' );

// Define colors into the editor palette.
add_theme_support( 'editor-color-palette',
    array(
		array( 'name' => __( 'Primary color', 'gear-of-web' ), 'slug'  => 'primary-color', 'color' => 'var(--primary-color)' ),
		array( 'name' => __( 'Secondary color', 'gear-of-web' ), 'slug'  => 'secondary-color', 'color' => 'var(--secondary-color)' )
	)
);

// Disable the custom colors.
add_theme_support( 'disable-custom-colors' );

// Define the font sizes into the editor.
add_theme_support( 'editor-font-sizes', array(
    array(
        'name' => esc_attr__( 'small', 'gear-of-web' ),
        'shortName' => esc_attr__( 'S', 'gear-of-web' ),
        'size' => 16,
        'slug' => 'small'
    ),
    array(
        'name' => esc_attr__( 'large', 'gear-of-web' ),
        'shortName' => esc_attr__( 'L', 'gear-of-web' ),
        'size' => 26,
        'slug' => 'large'
    ),
    array(
        'name' => esc_attr__( 'extra large', 'gear-of-web' ),
        'shortName' => esc_attr__( 'XL', 'gear-of-web' ),
        'size' => 28,
        'slug' => 'larger'
    )
) );

// Disable the custom font sizes.
add_theme_support('disable-custom-font-sizes');
