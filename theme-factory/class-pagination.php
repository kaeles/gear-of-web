<?php

/**
 * Gear of Web Pagination
 *
 * Builds pagination for archive or comments.
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @since 		Gear_of_Web 0.6
 * @license 	GPL2 - <https://www.gnu.org/licenses/gpl-2.0.html>
 * @author 		Rolling Web <contact@rolling-web.fr>
 */

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security.

class GOF_Pagination {

	/**
	 * Pagination for post types.
	 */
	public static function post_pagination() {
		if ( is_singular() ) {
			return;
		}

		global $wp_query;

		/** Stop execution if there's only 1 page */
		if ( $wp_query->max_num_pages <= 1 ) {
			return;
		}

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );

		/** Add current page to the array */
		if ( $paged >= 1 ) {
			$links[] = $paged;
		}

		/** Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		echo '<nav aria-label="Page navigation" class="pagination-wrapper"><ul class="pagination">' . "\n";

		/** Previous Post Link */
		if ( get_previous_posts_link() ) {
			printf( '<li class="page-item">%s</li>' . "\n", get_previous_posts_link() );
		}

		/** Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active page-item"' : ' class="page-item"';
			printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) ) {
				echo '<li class="page-item">…</li>';
			}
		}

		/** Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active page-item"' : ' class="page-item"';
			if ( $paged == $link ) {
				printf( '<li%s><span class="page-link">%s</span></li>' . "\n", $class, $link );
			} else {
				printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
			}
		}

		/** Link to last page, plus ellipses if necessary */
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) ) {
				echo '<li class="page-item disabled"><span class="page-link">…</span></li>' . "\n";
			}
			$class = $paged == $max ? ' class="active page-item"' : ' class="page-item"';
			printf( '<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}

		/** Next Post Link */
		if ( get_next_posts_link() ) {
			printf( '<li class="page-item">%s</li>' . "\n", get_next_posts_link() );
		}

		echo '</ul></nav>' . "\n";
	}

	/**
	 *
	 */
	public static function comment_pagination() {
		$pagination = paginate_comments_links( ['echo' => false, 'type' => 'array'] );
		$output = '';
		if ( is_array( $pagination ) && count( $pagination ) > 1 ) {
			foreach ( $pagination as $i => &$page ) {
				if ( false !== strpos( $page, 'current' ) ) {
					$page = str_replace( 'current', 'active', $page );
				}
				if ( false !== strpos( $page, 'page-numbers' ) ) {
					$page = str_replace( 'page-numbers', 'page-link', $page );
				}
				$page = "\n<li class='page-item'>$page</li>\n";
				$output .= $page;
			}
			unset( $page );

			echo '<nav class="pagination-wrapper" aria-label="Comment navigation">';
				echo '<ul class="pagination">';
					echo $output;
				echo '</ul>';
        	echo '</nav>';
		}
	}

	public static function posts_link_attributes( $attributes ) {
    	return 'class="page-link"';
	}
}

// END OF FILE
