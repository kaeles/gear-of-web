<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' ); // security.

/**
 * Customize API: WP_Customize_Color_Control class
 *
 * Adds new section, setting and control to the Customizer.
 *
 * @package WordPress
 * @subpackage Gear_of_Web
 * @since Gear_of_Web 0.4
 */

/**
 * Customize Color Control class.
 *
 * @since Gear_of_Web 0.6
 *
 * @see WP_Customize_Manager
 */
class GOF_Theme_Colors_Customizer {

	/**
	 * Instantiate the Theme colors customizer.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public static function init() {
		// register customizer options.
		add_action( 'customize_register', array( 'GOF_Theme_Colors_Customizer', 'register' ) );

		// set a new transient after customizer has saved new settings.
		add_action( 'customize_save_after', array( 'GOF_Theme_Colors_Customizer', 'save_transient' ) );
	}

	/**
	 * Register customizer options.
	 *
	 * Adds a new section named "Theme colors settings".
	 * In this section, add the theme color settings.
	 * Each setting is controlled by the hex color sanitizer.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @param WP_Customize_Manager $wp_customize WP Customizer object.
	 *
	 * @return void
	 */
	public static function register( $wp_customize ) {
		// adds new section.
		$wp_customize->add_section( 'theme_colors_settings', array(
			'title' => _x( 'Theme Colors Settings', 'panel title in customizer', 'gear-of-web' ),
			'priority' => 40,
		) );

		$theme_colors = array();
		// Background Color
		$theme_colors[] = array(
			'slug'          => 'gof_theme_background_color',
			'default'       => '#1f2124',
			'label'         => _x( 'Background color', 'option label in customizer', 'gear-of-web' ),
			'description'   => _x( 'The background color of the site is black by default for eco-design reasons.', 'option description in customizer', 'gear-of-web' )
		);
		// Primary Color
		$theme_colors[] = array(
			'slug'          => 'gof_theme_color_primary',
			'default'       => '#007bff',
			'label'         => _x( 'Primary color', 'option label in customizer', 'gear-of-web' ),
			'description'   => _x( 'This color will be use for general element.', 'option description in customizer', 'gear-of-web' )
		);
		// Secondary Color
		$theme_colors[] = array(
			'slug'          => 'gof_theme_color_secondary',
			'default'       => '#6c757d',
			'label'         => _x( 'Secondary color', 'option label in customizer', 'gear-of-web' ),
			'description'   => _x( 'This color will be use for general element.', 'option description in customizer', 'gear-of-web' )
		);

		foreach( $theme_colors as $i => &$color ) {
			// adds new setting
			$wp_customize->add_setting(
				$color['slug'],
				array(
					'default'           => $color['default'],
					'sanitize_callback' => 'sanitize_hex_color',
					'capability'        => 'edit_theme_options',
				)
			);
			// adds new control.
			$wp_customize->add_control(
				new WP_Customize_Color_Control(
					$wp_customize,
					$color['slug'],
					array(
						'label'         => $color['label'],
						'section'       => 'theme_colors_settings',
						'settings'      => $color['slug'],
						'description'   => $color['description'],
					)
				)
			);
		}
		unset( $color ); // destroy reference on the last element.
	}

	/**
	 * Set a new transient after customizer has saved new settings.
	 *
	 * The new transient has no expiration time and is overridden each time this method has called.
	 *
	 * @access public
	 * @static
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return void
	 */
	public function save_transient() {
		$theme_colors = self::generate_custom_colors_variables();

		$return = set_transient( 'gof_inline_styles', $theme_colors, 0 ); // set_transient: 0 for no expiration.

		if ( ! $return ) {
			$errors = new WP_Error();
			$message =_x( 'It appears something goes wrong with the transient saving.', 'error log for developers', 'gear-of-web' );
			$errors->add( 'gof-transient-saving', $message );
		}
	}

	/**
	 * Generate color variables.
	 *
	 * Adjust text colors values of the CSS variables depending on choosen theme colors.
	 *
	 * @access public
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return string $theme_css :root CSS variables.
	 */
	private static function generate_custom_colors_variables() {
		$background_color 	= sanitize_hex_color( get_theme_mod( 'gof_theme_background_color' ), '#1f2124' );
		$primary_color		= sanitize_hex_color( get_theme_mod( 'gof_theme_color_primary' ), '#007bff' );
		$secondary_color	= sanitize_hex_color( get_theme_mod( 'gof_theme_color_secondary' ), '#6c757d' );
		$link_colors 		= self::get_readable_link_color( $background_color );

		$theme_css = ':root{';

		$theme_css .= '--body--background-color:' . $background_color . ';';
		$theme_css .= '--body-text-color:' . self::get_readable_color( $background_color ) . ';';
		$theme_css .= '--primary-color:' . $primary_color . ';';
		$theme_css .= '--primary-text-color:' . self::get_readable_color( $primary_color ) . ';';
		$theme_css .= '--secondary-color:' . $secondary_color . ';';
		$theme_css .= '--secondary-text-color:' . self::get_readable_color( $secondary_color ) . ';';
		$theme_css .= '--primary-rgb:' . self::get_rgb_color( $primary_color, true ) . ';';
		$theme_css .= '--secondary-rgb:' . self::get_rgb_color( $secondary_color, true ) . ';';
		$theme_css .= '--link-color:' . $link_colors['color-link'] . ';';
		$theme_css .= '--visited-link-color:' . $link_colors['color-link-visited'] . ';';

		$theme_css .= '}';

		return $theme_css;
	}

	/**
	 * Determine the luminance of the given color and then return #fff or #000 so that the text is always readable.
	 *
	 * @access private
	 *
	 * @param string $theme_color One of the theme colors.
	 *
	 * @since Gear_of_Web 0.6
	 *
	 * @return string (hex color)
	 */
	private static function get_readable_color( $theme_color ) {
		// Remove the "#" symbol from the beginning of the color.
		$hex = ltrim( $theme_color, '#' );

		// Make sure there are 6 digits for the below calculations.
		if ( 3 === strlen( $hex ) ) {
			$hex = substr( $hex, 0, 1 )
			. substr( $hex, 0, 1 )
			. substr( $hex, 1, 1 )
			. substr( $hex, 1, 1 )
			. substr( $hex, 2, 1 )
			. substr( $hex, 2, 1 );
		}

		// Get red, green, blue.
		$red   = hexdec( substr( $hex, 0, 2 ) );
		$green = hexdec( substr( $hex, 2, 2 ) );
		$blue  = hexdec( substr( $hex, 4, 2 ) );

		// Calculate the luminance.
		$lum = ( 0.2126 * $red ) + ( 0.7152 * $green ) + ( 0.0722 * $blue );
		$lum = (int) round( $lum );

		// Return the readable color.
		return ( 127 < $lum ) ? '#000' : '#fff';
	}

	/**
	 * Determine the color of the links from the background color of the site
	 *
	 * @access private
	 *
	 * @param $theme_color One of the theme colors.
	 *
	 * @since Gear_of_Web 0.7
	 *
	 * @return array of hex colors
	 */
	private static function get_readable_link_color( $theme_color ) {
		$link_colors = array();

		// Remove the "#" symbol from the beginning of the color.
		$hex = ltrim( $theme_color, '#' );

		// Make sure there are 6 digits for the below calculations.
		if ( 3 === strlen( $hex ) ) {
			$hex = substr( $hex, 0, 1 )
			. substr( $hex, 0, 1 )
			. substr( $hex, 1, 1 )
			. substr( $hex, 1, 1 )
			. substr( $hex, 2, 1 )
			. substr( $hex, 2, 1 );
		}

		// Get red, green, blue.
		$red   = hexdec( substr( $hex, 0, 2 ) );
		$green = hexdec( substr( $hex, 2, 2 ) );
		$blue  = hexdec( substr( $hex, 4, 2 ) );

		// Calculate the luminance.
		$lum = ( 0.2126 * $red ) + ( 0.7152 * $green ) + ( 0.0722 * $blue );
		$lum = (int) round( $lum );

		// Determine the colors.
		if ( 127 > $lum ) {
			$link_colors['color-link'] = '#8ab4f8';
			$link_colors['color-link-visited'] = '#c58af9';
		} else {
			$link_colors['color-link'] = '#1a0dab';
			$link_colors['color-link-visited'] = '#609';
		}

		// Return the readable color.
		return $link_colors;
	}

	/**
	 * Convert a hexa decimal color code to its RGB equivalent
	 *
	 * @param string $hexStr (hexadecimal color value)
	 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
	 */
	private static function get_rgb_color( $hexStr, $returnAsString = false, $seperator = ',' ) {
		$hexStr = preg_replace( '/[^0-9A-Fa-f]/', '', $hexStr ); // Gets a proper hex string
		$rgbArray = array();
		if ( strlen($hexStr) == 6 ) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec( $hexStr );
			$rgbArray['red'] 	= 0xFF & ( $colorVal >> 0x10 );
			$rgbArray['green'] 	= 0xFF & ( $colorVal >> 0x8 );
			$rgbArray['blue'] 	= 0xFF & $colorVal;
		} elseif ( strlen( $hexStr ) == 3 ) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] 	= hexdec( str_repeat( substr( $hexStr, 0, 1 ), 2 ) );
			$rgbArray['green'] 	= hexdec( str_repeat( substr( $hexStr, 1, 1 ), 2 ) );
			$rgbArray['blue'] 	= hexdec( str_repeat( substr( $hexStr, 2, 1 ), 2 ) );
		} else {
			return false; //Invalid hex color code
		}
		return $returnAsString ? implode( $seperator, $rgbArray ) : $rgbArray; // returns the rgb string or the associative array
	}
}

// END OF FILE
