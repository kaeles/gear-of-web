<?php

/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Gear_Of_Web
 * @since Gear_Of_Web 0.1
 */

/*
* If the current post is protected by a password and the visitor has not yet
* entered the password we will return early without loading the comments.
*/
if ( post_password_required() ) return; ?>

<?php if ( have_comments() ) : ?>
	<div class="comments-section">
		<h2 class="comments-title">
			<?php
			printf(
				/* translators:
				* %1$s is the number of comment internationalized.
				* %2$s is the post title.
				*/
				_nx( '%1$s thought on "%2$s"', '%1$s thoughts on "%2$s"', get_comments_number(), 'comments title', 'gear-of-web' ),
				number_format_i18n( get_comments_number() ),
				'<span>' . get_the_title() . '</span>'
			);
			?>
		</h2>

		<div class="comments">
			<?php
			add_filter( 'comment_reply_link', array( 'GOF_Walker_Comments', 'filter_comment_reply_button' ) );
			wp_list_comments( array(
				'short_ping' 	=> 'ul',
				'style'       	=> 'div',
				'avatar_size' 	=> 42,
				'format' 		=> 'html5',
				'walker' 		=> new GOF_Walker_Comments()
			) );
			?>
		</div><!-- .comments -->
		<?php GOF_Pagination::comment_pagination(); ?>
		<?php add_filter( 'comment_form_submit_button', array( 'GOF_Walker_Comments', 'filter_comment_form_submit_button' ), 10, 2 ); ?>
		<?php comment_form( GOF_Walker_Comments::get_comment_args() ); ?>
	</div>

	<?php endif; // have_comments() ?>


<?php

// END OF FILE
