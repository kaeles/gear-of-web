<?php

/**
 * The template for displaying archive pages (taxonomies)
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since Gear_Of_Web 0.1
 */
get_header(); ?>

<main id="post-<?php the_ID() ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/title' );

	get_template_part( 'template-parts/terms-suggestions' );

	the_archive_description( '<div class="archive-description">', '</div>' );

	get_template_part( 'template-parts/loop', 'stickies-posts' );

	get_template_part( 'template-parts/loop', 'main' );
	?>

</main>

<?php get_footer();

// END OF FILE
