<?php
/**
 * This file initialize the Gear of Web theme.
 *
 * The Gear of Web theme is a minimal template for WordPress.
 * It embeds Bootstrap and other small libraries allowing everyone to create a website with the minimum of technical knowledge.
 * This theme is not perfect, but the improvement is meant to be continuous.
 * Please contact the theme editor if you have a problem or want to share any recommendations.
 *
 * @package 	WordPress
 * @subpackage 	Gear_of_Web
 * @license 	http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author 		Rolling Web <contact@rolling-web.fr>
 */

/**
 * Register the __autoload() implementation
 */
function gof_autoloader( $class_name ) {
	$namespace = 'GOF_';

	if ( strpos( $class_name, $namespace ) !== 0) {
		return;
	}

	$class_file = str_replace( $namespace, 'class-', $class_name );
	$class_file = str_replace( '_', '-', $class_file ) . '.php';

	$directory = get_template_directory();
	$path = $directory . DIRECTORY_SEPARATOR . 'theme-factory' . DIRECTORY_SEPARATOR . strtolower( $class_file );

	if ( file_exists( $path ) ) {
		require_once( $path );
	}
}
spl_autoload_register( 'gof_autoloader' );

if ( ! function_exists( 'gof_init' ) ) {
	/**
	 * Init the Gear_of_Web theme.
	 * This procedure calls two files.
	 * ./theme-factory/class-gof-theme.php 		setup the theme using WP standard procedures.
	 * ./init-seo-improvments.php 	initialize the SEO improvments. WP isn't perfect in this regard
	 * 								so we want to change the default behavior of the core which sometimes generates unoptimized content.
	 *
	 * @uses after_setup_theme WP hook
	 */
	function gof_init() {
		// Init theme.
		GOF_Theme::init();
	}
}
add_action( 'after_setup_theme', 'gof_init' );

// END OF FILE
