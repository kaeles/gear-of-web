<?php

/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since Gear_Of_Web 0.1
 */
get_header(); ?>

<main id="page-404" <?php post_class( 'hentry' ); ?>>

	<?php get_template_part( 'template-parts/404', 'content' ); ?>

</main>

<?php get_footer();

// END OF FILE
