<?php
/**
 * The template for displaying Search pages (Returns posts containing keyword)
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since Gear_Of_Web 0.1
 */
get_header(); ?>

<main id="single-<?php the_ID() ?>" <?php post_class() ?>>

	<?php get_template_part( 'template-parts/search', 'content' ); ?>

</main>

<?php get_footer();

// END OF FILE
