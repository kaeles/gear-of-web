<?php
/**
 * Page template
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since RW Theme 0.1
 */
get_header(); ?>

<main id="page-<?php the_ID() ?>" <?php post_class() ?>>

	<?php get_template_part( 'template-parts/page', 'content' ); ?>

</main>

<?php get_footer();

// END OF FILE
