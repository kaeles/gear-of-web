<?php
/**
 * This file generates head tags and the site header navigation bar.
 *
 * @package WordPress
 * @subpackage Gear_Of_Web
 * @since Gear_Of_Web 0.1
 */ ?>

<!Doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class( 'gof' ); ?>>
	<?php wp_body_open(); ?>
	<header id="masthead" class="site-header">
		<?php if ( has_custom_logo() ) : ?>
			<?php the_custom_logo(); ?>
		<?php else : ?>
			<a class="site-title" rel="home" href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo('name'); ?></a>
		<?php endif; ?>
		<nav role="navigation" id="site-navigation" class="site-navigation">
			<button class="menu-toggle" type="button" aria-controls="primary-menu" aria-expanded="false">
				<span class="dashicons dashicons-menu"></span>
			</button>
			<div class="menu-wrapper">
				<?php
				if ( has_nav_menu( 'header-menu' ) ) {
					wp_nav_menu( array(
						'theme_location' 	=> 'header-menu',
						'menu_id' 			=> 'primary-menu',
						'container'			=> false,
						'walker'        	=> new GOF_Walker_Nav_Menu(),
						)
					);
				}
				if ( is_active_sidebar( 'main-menu-sidebar-1' ) ) : ?>
					<div id="extra-menu" class="sidebar-menu">
						<?php dynamic_sidebar( 'main-menu-sidebar-1' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</nav>
	</header>

<?php

// END OF FILE
