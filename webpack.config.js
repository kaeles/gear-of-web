const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
  return {
    ...(argv.mode === "development" ? { watch: true } : {}),
    watchOptions: {
      aggregateTimeout: 600,
      ignored: /node_modules/,
    },
    entry: {
      "front": ["/src/js/gof.js", "./src/css/style.scss"],
      "editor": ["/src/js/editor-scripts.js", "/src/css/editor-styles.scss"],
      "gof-admin": ["/src/js/admin-gof.js", "/src/css/admin-styles.scss"]
    },
    output: {
      filename: argv.mode === "production" ? "[name].min.js" : "[name].js",
      path: path.resolve(__dirname, "dist"),
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: argv.mode === "production" ? "[name].min.css" : "[name].css",
      }),
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
      }),
    ],
    externals: {
      jquery: "jQuery",
    },
    module: {
      rules: [
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          exclude: /node_modules/,
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "images/",
          },
        },
        {
          test: /\.(eot|woff|woff2|ttf)$/,
          exclude: /node_modules/,
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "fonts/",
          },
        },
        {
          test: /\.scss$/i,
          exclude: /node_modules/,
          use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "postcss-loader",
            "sass-loader",
          ],
        },
      ],
    },
  };
};
