<?php
/**
 * Single template (post)
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since Gear_Of_Web 0.1
 */
get_header(); ?>

<main id="single-<?php the_ID() ?>" <?php post_class() ?>>

	<?php get_template_part( 'template-parts/single', 'content' ); ?>

</main>

<?php get_footer();

// END OF FILE
