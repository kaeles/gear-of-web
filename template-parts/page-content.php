<?php if ( have_posts() ) : ?>

	<article class="page-content">
		<?php get_template_part( 'template-parts/title' ); ?>
		<?php the_content(); ?>
		<?php comments_template(); ?>
	</article>

	<?php get_template_part( 'template-parts/page', 'sidebar' ); ?>

<?php endif;

//END OF FILE
