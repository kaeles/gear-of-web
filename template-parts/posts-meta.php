<?php
echo '<details open>';
    printf(
        '<p><span itemprop="author">%1$s</span> %2$s <time itemprop="datePublished" datetime="%3$s">%4$s</time> %2$s <span itemprop="about">%5$s</span></p>',
        get_the_author_meta( 'display_name', $post->post_author ),
        ' | ',
        get_the_date( 'c' ),
        get_the_date(),
        get_the_category()[0]->name
    );
echo '</details>';