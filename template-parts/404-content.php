<article class="content-404">

    <?php get_template_part( 'template-parts/title' ); ?>
    <section class="notice">
        <p>
            <?php
            _ex( 
                'It looks like nothing was found at this location. Below is a list of articles that made the community react.',
                'title of the page 404.',
                'gear-of-web'
            );
            ?>
        </p>
    </section>

    <?php get_template_part( 'template-parts/single', 'suggestions' ); ?>

</article>