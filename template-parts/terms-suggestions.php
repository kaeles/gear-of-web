<?php
if ( is_home() ) {
	GOF_Terms::the_main_terms();

}

if ( is_tax() || is_category() ) {
	$q_object = get_queried_object();
	$terms = GOF_Terms::the_terms_children( $q_object );

}

?>