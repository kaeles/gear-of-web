<?php query_posts(array(
	'orderby' => 'comment_count',
	'showposts' => 6,
	'ignore_sticky_posts' => true
)); ?>

<?php get_template_part( 'template-parts/loop', 'main' ); ?>

<?php wp_reset_query(); ?>
