<?php if ( have_posts() ) : ?>

	<article class="post-content" itemscope itemtype="https://schema.org/Article">
		<?php get_template_part( 'template-parts/title' ); ?>
		<?php the_post_thumbnail( 'full', ['class' => 'img-fluid'] ); ?>
		<?php the_content(); ?>
		<?php comments_template(); ?>
	</article>

	<?php get_template_part( 'template-parts/single', 'sidebar' ); ?>

<?php else : ?>

	<div class="alert alert-warning" role="alert">
		<?php _ex( 'Sorry, no posts matched your criteria.', 'displayed if no post found', 'gear-of-web' ); ?>
	</div>

<?php endif;

// END OF FILE
