<?php

/**
 * Display title pages.
 * Take in consideration single, category, shop and main pages.
 * Ignore tags, authors, years, months and days terms.
 * Theses pages are deactivated within the theme.
 */

	echo '<header class="page-title" itemprop="headline">';
		echo '<h1 itemprop="name">';

		if ( is_home() && get_option( 'page_for_posts' ) ) {
			echo get_the_title( get_option( 'page_for_posts' ) );

		} elseif ( is_category() || is_archive() ) {
			single_cat_title( '', true );

		} elseif ( is_single() && have_posts() || is_page() || is_front_page() ) {
			the_title();

		} elseif ( is_404() ) {
			_ex( '404: Not Found', '404 page main title', 'gear-of-web' );

		} elseif ( is_search() ) {
			_ex( 'Search', 'Search page main title', 'gear-of-web' );

		} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
			if ( class_exists( 'Woocommerce' ) && !is_single() ) {
				echo get_post( woocommerce_get_page_id( 'shop' ) )->title;

			}
		} elseif ( function_exists('jigoshop_init') && !is_single() ) {
			echo get_post( jigoshop_get_page_id( 'shop' ) )->title;

		}

		echo '</h1>';
	
		GOF_Theme::display_breadcrumb();

		if ( is_single() ) {
			get_template_part( 'template-parts/posts', 'meta' );

		}

	echo '</header>';
