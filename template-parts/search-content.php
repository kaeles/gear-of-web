<article class="content-404">

	<?php get_template_part( 'template-parts/title' ); ?>
	<section class="notice">
		<p>
			<?php
			printf(
			/* translators: %s is the search term. */
			_x(
				'Here are the results associated with the term "%s" you are looking for.',
				'explanation of the search results section.',
				'gear-of-web' ),
			get_search_query()
			);
			?>
		</p>
	</section>

	<?php get_template_part( 'template-parts/loop', 'main' ); ?>
</article>