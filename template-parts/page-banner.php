<?php if ( has_post_thumbnail() ) : ?>
	<header class="page-banner">
		<?php the_post_thumbnail( 'page-banner', ['class' => 'img-fluid d-none d-lg-block'] ); ?>
	</header>
<?php endif; ?>
