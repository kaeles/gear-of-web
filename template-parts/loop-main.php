<?php if ( have_posts() ) : ?>
	<section class="loop-posts">
		<?php while ( have_posts() ) : the_post(); ?>
			<article class="loop-post">
				<?php
				if ( has_post_thumbnail() ) :
					get_the_post_thumbnail(
						get_the_ID(),
						'post-thumb',
						['class' => 'post-thumb']
					);
				endif;
				?>
				<div class="post-body">
					<?php the_title( '<h2 class="post-title">', '</h2>' ); ?>
					<?php the_excerpt(); ?>
				</div>
				<div class="post-footer">
					<a class="btn btn-primary" title="<?php echo esc_attr( get_the_title() ) ?>" href="<?php the_permalink() ?>">
						<?php _ex( 'Read More',
						'Label used for blog buttons',
						'gear-of-web' ) ?>
					</a>
					<div class="meta-info">
						<small class="time"><?php echo GOF_THEME::get_time_diff( get_the_ID() ); ?></small>
						<small class="post-author">
							<?php
							printf(
								/* translators: %s is the author name. */
								_x( 'By %s',
								'Author tag for articles in the main article loop.',
								'gear-of-web' ),
								get_the_author()
							); ?>
						</small>
					</div>
				</div>
			</article>
		<?php endwhile; ?>
	</section>
	<?php get_template_part( 'template-parts/loop-footer' ); ?>
<?php endif;
