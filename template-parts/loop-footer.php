<section class="loop-footer">
    <?php GOF_Pagination::post_pagination(); ?>
    <div class="search-form-wrapper">
    <?php
        printf(
            '<p class="search-notice">%1$s</p>',
            _x( 'Do you want to perform a relevance search?',
            'Displayed with the search form on the 404 page',
            'gear-of-web' )
        );
        get_search_form();
    ?>
    </div>
</section>