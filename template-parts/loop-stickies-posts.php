<?php $stickies = gof_fetch_stickies_posts() ?>
<?php if ( $stickies->have_posts() && get_query_var( 'paged' ) === 0 ) : ?>
	<section class="loop-stickies">
		<?php while ( $stickies->have_posts() ) : $stickies->the_post(); ?>
			<article class="loop-sticky">
				<?php
				if ( has_post_thumbnail() ) :
					get_the_post_thumbnail( get_the_ID(), 'post-thumb', ['class' => 'sticky-thumb'] );
				endif;
				?>
				<div class="sticky-body">
					<?php the_title( '<h2 class="sticky-title">', '</h2>' ); ?>
					<div class="sticky-excerpt"><?php the_excerpt(); ?></div>
				</div>
				<div class="sticky-footer">
					<a class="btn btn-primary" title="<?php echo esc_attr( get_the_title() ) ?>" href="<?php the_permalink() ?>">
						<?php _ex( 'Read More',
						'Label used for blog buttons',
						'gear-of-web' ) ?>
					</a>
					<div class="meta-info">
						<small class="time"><?php echo GOF_THEME::get_time_diff( get_the_ID() ); ?></small>
						<small class="post-author">
							<?php
							printf(
								/* translators: %s is the author name. */
								_x( 'By %s',
								'Author tag for articles in the main article loop.',
								'gear-of-web' ),
								get_the_author()
							); ?>
						</small>
					</div>
				</div>
			</article>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>
