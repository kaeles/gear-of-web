<?php
/**
 * The template for displaying footer section area.
 *
 * @package WordPress
 * @subpackage Gear_Of_Web
 * @since Gear_Of_Web 0.1
 */ ?>

<footer id="colophon" class="site-footer">

	<?php if ( is_active_sidebar( 'top-footer' ) ) : ?>

		<div class="top-footer">
			<?php dynamic_sidebar( 'top-footer' ); ?>
		</div>

	<?php endif; ?>

	<?php if ( is_active_sidebar( 'bottom-footer' ) ) : ?>

		<div class="bottom-footer">
			<?php do_action( "gof_bottom_footer" ); ?>
		</div>

	<?php endif; ?>

</footer>

<?php wp_footer(); ?>

</body></html>

<?php

// END OF FILE
