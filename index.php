<?php
/**
 * Template used by default by WordPress is no other more specific template was found.
 *
 * @package WordPress
 * @subpackage GOF_THEME
 * @since Gear_Of_Web 0.1
 */
get_header(); ?>


	<?php if ( is_home() && get_option( 'page_for_posts' ) ) : ?>

		<main id="blog-page"  <?php post_class( 'site-blog' ); ?>>

			<?php

			get_template_part( 'template-parts/title' );

			get_template_part( 'template-parts/terms-suggestions' );

			get_template_part( 'template-parts/loop', 'stickies-posts' );

			get_template_part( 'template-parts/loop', 'main' );

			?>

		</main>

	<?php else : ?>

		<main id="post-<?php the_ID() ?>"  <?php post_class(); ?>>

			<?php GOF_Theme::the_banner_heading( get_queried_object() );?>

			<?php get_template_part( 'template-parts/loop', 'main' ); ?>

		</main>

	<?php endif; ?>

<?php get_footer();

// END OF FILE
